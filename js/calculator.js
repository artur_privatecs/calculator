/**
 * Main module
 * @param {object} Module
 * @returns Module
 */
(function(Module){
    
    //private variables
    var _allowedChars = '0123456789+-/*.';
    
    /**
     * Calculator class
     * @param {string} selector jQuery selector for wrapper
     * @returns {object}
     */
    Module.Calculator = function(selector){
        this.wrappSelector = selector;
    };
    
    var p = Module.Calculator.prototype;
    
    p.constructor = Module.Calculator;
    
    //jQuery selectors, you can override it before run init()
    p.wrappSelector = null;
    p.ioSelector = '.io';
    p.btnSelecor = '.btn';
    
    //jQuery objects
    p.wrapp = null;
    p.io = null;  
    
    //inner properties
    p.expression = '';
    p.number = '';
    p.commaFlag = false;
    p.operationFlag = false;
    
    /**
     * Initilize calculator
     * @returns {undefined}
     */
    p.init = function(){  
        this.wrapp = $(this.wrappSelector);
        this.io = this.wrapp.find(this.ioSelector);
        this.observe();        
    };
    
    /**
     * Keep all event handlers
     * @returns {undefined}
     */
    p.observe = function(){
        
        var self = this;
        
        this.wrapp.find('.btn').click(function(){
            self.mapButtons(this);
        });
    };
    
    /**
     * Map buttons to actions
     * @param {object} button object with type and value
     * @returns {undefined}
     */
    p.mapButtons = function(button){
                
        switch(button.getAttribute('data-type'))
        {
            case 'number':
                if(_allowedChars.indexOf(button.value) > -1)
                {
                    this.addNumber(button.value);
                }
                break;

            case 'operation':
                if(_allowedChars.indexOf(button.value) > -1)
                {
                    this.addOperation(button.value);
                }                    
                break;

            case 'clear':
                this.clear();
                break;

            case 'comma':
                this.addComma();
                break;

            case 'result':
                this.execute();
                break;

            default:                
        }     
    };
    
    /**
     * Clear I/O field end reset all variables   
     * @returns {undefined}
     */
    p.clear = function(){
        this.io.attr('value', 0);
        this.expression = '';
        this.number = '';
        this.commaFlag = false;
        this.operationFlag = false;        
    };
    
    /**
     * Execute expression and render result
     * @returns {undefined}
     */
    p.execute = function(){
        //cut out last comma/operation
        if(this.operationFlag)
        {
            this.expression = this.expression.slice(0, this.expression.length-1);
        }            
        else
        {
            this.expression = eval(this.expression + parseFloat(this.number)).toString();
        }
        
        this.commaFlag = (this.expression.indexOf('.') > -1) ? true : false;
        this.operationFlag = false;
        this.number = '';
        this.render(this.expression);
    };    
    
    /**
     * Add number to expression
     * @param {string} number
     * @returns {Boolean} true if number added
     */
    p.addNumber = function(number){
                
        this.number += number;        
        this.operationFlag = false;
        this.render(this.expression + this.number);
    };
    
    /**
     * Add operation to expression
     * @param {string} operation one of allowed operations
     * @returns {Boolean} true if success
     */
    p.addOperation = function(operation){
        
        if(this.operationFlag)
        {
            //replace operation if there is something already
            this.expression = this.expression.slice(0, this.expression.length-1)+operation;
        }
        else
        {
            if(this.number.length)
                this.expression += parseFloat(this.number);
            
            this.expression += operation;
        }   
       
        this.number = '';        
        this.operationFlag = true;
        this.commaFlag = false;
        this.render(this.expression);
    };
    
    /**
     * Add comma to expression
     * @returns {Boolean} true if success
     */
    p.addComma = function(){
        
        if(!this.commaFlag)
        {            
            if(!this.number.length)
                this.number += '0';

            this.number += '.';
            this.commaFlag = true;
            this.render(this.expression+this.number);
        }
    };
    
    /**
     * Output value to I/O field
     * @param {string} value
     * @returns {undefined}
     */
    p.render = function(value){
        this.io.attr('value', value);
    };
    
    return Module;
    
})(window.Module = window.Module || {});