$(document).ready(function(){
    
    /**
     * We can init multiplie calculators on one page,
     * but we need create new DOM elements for each calculator 
     * and pass wrapp selector to constructor.
     */
    var calculator = new Module.Calculator('#calculator');    
    calculator.init();
});